gluon-ffbsnext-debugrsyslog
===========================

This package manages the `rsyslog` config in the `parker` net.

Using this package all nodes in the `beta`- or `experimental`-branches will
send their syslog to us.

Nodes in the `stable`-branch will not send syslog.
