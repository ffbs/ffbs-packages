gluon-ffbsnext-info-pull
========================

This package is used to make a node owner's contact into available to
the Freifunk Braunschweig admins.

At Freifunk Braunschweig we do not publish contact info on the node's
status page - per default.

The user can choose one of the following options:

1) User does not share any contact info.
2) User shares contact info with Freifunk Braunschweig.
3) User shows contact info on the status page.

In case 2) and 3) this package makes it possible to collect the contact
info in a safe way.

How it works
------------

On each node there is an additional user with a known ssh public key
in their `authorized_keys`.
This key has a forced command that `cat`s the contact info to `stdout`
and then quits.

This way we have authorization and encryption using `ssh` and can pull
the infos from a node, if needed.

The private key is only accessbile to the Freifunk Braunschweig admins.
